#
# SPDX-FileCopyrightText: 2018-2020 Aleix Pol Gonzalez <aleixpol@kde.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import re
import argparse
import zipfile
import subprocess
import time

# Extract the internal application name from the APK given
def readApplicationName( apkPath ):
    # Prepare the aapt (Android SDK command) to inspect the provided APK
    commandToRun = "aapt dump badging %s" % (apkPath)
    manifest = subprocess.check_output( commandToRun, shell=True ).decode('utf-8')

    # Search through the aapt output for the name of the application
    result = re.search(' name=\'([^\']*)\'', manifest)
    return result.group(1)

# Main function for extracting metadata from APK files
def processApkFile( apkFilepath ):
    # Log the file that we are going to be working with
    print("doing...", apkFilepath)

    # First, determine the name of the application we have here
    # This is needed in order to locate the metadata files within the APK that have the information we need
    applicationName = readApplicationName( apkFilepath )

    # Check if we have metadata provided by the build process, and if so apply that
    zipName = os.path.join(arguments.fastlane, 'fastlane-' + applicationName + '.zip')
    if os.path.exists(zipName):
        print("using metadata from build process:", zipName)
        with zipfile.ZipFile(zipName, 'r') as zipFile:
            zipFile.extractall(os.path.join(arguments.fdroid_repository, 'metadata'))
    else:
        print("missing metadata package!", zipName)

### Script Commences

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to update an F-Droid repository using metadata contained within APKs')
parser.add_argument('--fdroid-repository', type=str, required=True)
parser.add_argument('--fastlane', type=str, required=True)
arguments = parser.parse_args()

# First off, we should make sure the repository has been setup
# The path we're given should be to the folder which contains the repo/ folder
if not os.path.exists( arguments.fdroid_repository ) or not os.path.exists( arguments.fdroid_repository + '/repo/' ):
    print("The specified F-Droid repository does not exist")
    print("Please run 'fdroid init' to setup the repository then try again")
    sys.exit(1)

# Now that we know we have a valid F-Droid repository, we need to get F-Droid to prepare the metadata skeletons for us
# We'll then fill in these templates with the information from the APK files (a list of which we'll be gathering next)
subprocess.check_call("fdroid update -c", shell=True, cwd=arguments.fdroid_repository)

# With the skeletons now available, it's time to get our list of APK files to work on ready
# For this, we go over the repo/ subdirectory mentioned above, looking at all *.apk files
knownApks = []
for entry in os.scandir( arguments.fdroid_repository + '/repo/' ):
    # First, we should ensure the item we've got is an actual file
    # If it is anything else, we can skip it
    if not entry.is_file(follow_symlinks=False):
        continue

    # Now we need to make sure the filename is what we expect it to be
    # If the file in question does not end with *.apk then we can ignore it
    if not entry.name.endswith('.apk'):
        continue

    # purge ancient APKs
    # those occur for example due to renames or removed builds
    if os.path.getmtime(entry.path) + (3600 * 24 * 60) < time.time():
        print("removing outdated package:", entry.name)
        os.remove(entry.path)
        continue

    # Now we know we have a file that we are interested in
    knownApks.append( entry.path )

# With the list of APK files known to us now, we can go ahead and start extracting the metadata from those APK files
# This metadata will be written into the appropriate places in the F-Droid repository
# To ensure this process completes quickly, even with a large number of potential APKs we use worker threads to do this
for apkFile in knownApks:
    # Process it
    processApkFile( apkFile )

# Finally we ask F-Droid to do a full update pass
# This syncs the metadata we just updated/prepared into the actual F-Droid repository which will be used by F-Droid clients
subprocess.check_call("fdroid update", shell=True, cwd=arguments.fdroid_repository)

# Last but not least, we publish the repository to production for clients to use
subprocess.check_call("fdroid deploy -v", shell=True, cwd=arguments.fdroid_repository)
