// Where will the craftmaster configuration be found (relative to the working directory that is)?
def craftmasterConfigDir = "binary-factory-tooling/craft/configs/prebuilt/"

// Request a node to be allocated to us
node( "macOS" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First we want to make sure Craft is ready to go
		stage('Preparing Craft') {
			// Make sure we start with a clean slate
			deleteDir()

			// Grab our tooling which we will need in a few moments
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/sysadmin/binary-factory-tooling.git']]
			]

			// We are also reliant on the CI System Tooling for parts of this, so grab it too
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'ci-tooling/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/sysadmin/ci-tooling.git']]
			]

			// Make sure that Craftmaster is ready to go
			sh """
				/usr/local/bin/python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository "https://invent.kde.org/packaging/craftmaster.git" --into ~/Craft/BC/
				/usr/local/bin/python3 "$WORKSPACE/bf-tooling/craft/checkout-repository.py" --repository "https://invent.kde.org/sysadmin/binary-factory-tooling.git" --into ~/Craft/BC/
			"""
		}

		// Now that is done, it's time to rebuild the Craft Cache!
		stage('Build Craft Cache') {
			sh """
				cd ~/Craft/BC/
				/usr/local/bin/python3 craftmaster/Craftmaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c -i craft
				/usr/local/bin/python3 craftmaster/Craftmaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c --add-blueprint-repository https://invent.kde.org/vonreth/craft-blueprints-prebuilt.git
				/usr/local/bin/python3 craftmaster/Craftmaster.py --config ${craftmasterConfigDir}/CraftBinaryCache.ini --target ${craftPlatform} --variables CreateCache=True UseCache=True -c --list-file ${craftmasterConfigDir}/BinaryCachePackages.list
			"""
		}
	}

	// We need to catch errors here to ensure that the cleanup always runs, even if the upload fails
	// While this is a bit wasteful it at least determines that errors in the publishing process leave a builder with a very full disk!
	catchError {
		// And finally, we'll upload it to the master server
		stage('Publish Craft Cache') {
			sh """
				cd "$WORKSPACE/ci-tooling/"
				/usr/local/bin/python3 helpers/sftp-directory-mirror.py --source ~/Craft/BC/cache/ --destination /srv/archives/files/craft/prebuilt/ --server deino.kde.org --username craft
			"""
		}
	}


	// Then we can cleanup everything we did
	stage('Cleaning Up') {
		sh """
			rm -rf ~/Craft/BC/
		"""
	}

	// As the macOS Slaves are permanent ones, we erase the Workspace as the last thing we do
	deleteDir()
}
}
