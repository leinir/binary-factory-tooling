// Request a node to be allocated to us
node( "MarbleVectorOSM" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// The image actually has these scripts baked into it...
			// But we prefer a direct Git clone to avoid having to rebuild the image everytime a change is made to them
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/education/marble.git']]
			]
		}

		// Let's build the VectorOSM Tile Generator First...
		stage('Building Tile Generator') {
			// We rely on a script shipped within Marble for this
			sh """
				tools/vectorosm-tilecreator/setup/build/build-tilegenerator.sh
				mv /output/usr/bin/marble-* \$WORKSPACE/
			"""
		}

		// Then we build the Tirex/mod_tile *.debs
		stage('Generating Tirex Packages') {
			// The scripts handle everything here, so just run them
			sh """
				tools/vectorosm-tilecreator/setup/build/build-tirex.sh
				mv /output/*.deb \$WORKSPACE/
			"""
		}

		// Finally we capture the binaries and *.deb's for usage on Telemid
		stage('Capturing Files') {
			// We use Jenkins artifacts for this to save having to setup additional infrastructure
			archiveArtifacts artifacts: '*.deb, marble-*', onlyIfSuccessful: true
		}
	}
}
}
